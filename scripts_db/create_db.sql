SET SQL_MODE = '';
DROP DATABASE IF EXISTS nmas;
CREATE DATABASE IF NOT EXISTS nmas;
USE nmas;

-- ********************************* Client ********************************!*
-- @field reg_id = Registry identification
-- @field cli_name = Client name
-- @field cli_type = Determines the type of client (Legal Person(L), Natural Person(N))
-- @field federal_reg = Cnpj(Federal Registry of Legal Person) or Cpf(Individual Taxpayer Registration - ITIN)
-- @field state_reg = IE(State Registry of Legal Person) or RG(General Registry of Natural Person)
-- @field reg_date = Client registration date
-- @field birth_date = Birth date or company opening date
-- @field situ = Client situation (Active(A), Inactive(I))
-- @field obs = Observation
-- @field last_buy = Last buy
-- @field overdue_debt = Overdue debt
-- @field last_update = Last update
-- @field conveyor_id = Shipping company identification
-- @field seller_id = Seller identification
-- @field pricelist_id = Price list identification 
-- @field cred_limit = Credit limit
-- @field used_limit = Used credit limit
-- @field pend_doc = Incomplete registry with pending documentation
-- @field extseller_id = External seller identification
-- @field sync = Indicates whether the client is synchronized with the online system: Synchronized(S), Included(I), Updated(U), Deleted(D)
-- @field fixed_day = Fixed payment days during the month: Format: 0510152025 = 05 - day 5, 10 - day 10...
-- @field salesarea_id = Identification of the sales area
-- @field paycond_id =  Identification of the client's preferred payment condition
-- @field payment_way = Client's preferred payment way
-- @field repres_id = Representative Identification
-- @field pro_dest = Product destination: 1 - Consumption, 2 - Resale, 3 - Industrialization
CREATE TABLE IF NOT EXISTS client(
	reg_id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	cli_name VARCHAR(50) NOT NULL DEFAULT "",
	cli_type CHAR(1) NOT NULL DEFAULT "",
	federal_reg VARCHAR(18) NOT NULL DEFAULT "",
	state_reg VARCHAR(18) NOT NULL DEFAULT "",
	reg_date DATE NOT NULL DEFAULT 0,
	birth_date DATE NOT NULL DEFAULT 0,
	situ CHAR(1) NOT NULL DEFAULT "",
	obs TEXT NULL,
	last_buy DATE NOT NULL DEFAULT 0,
	overdue_debt BOOL NOT NULL DEFAULT 0,
	last_update DATE NOT NULL DEFAULT 0,
	conveyor_id INTEGER NOT NULL DEFAULT 0,
	seller_id INTEGER NOT NULL DEFAULT 0,
	pricelist_id INTEGER NOT NULL DEFAULT 0,
	cred_limit FLOAT NOT NULL DEFAULT 0,
	used_limit FLOAT NOT NULL DEFAULT 0,
	pend_doc BOOL NOT NULL DEFAULT 0,
	extseller_id INTEGER NOT NULL DEFAULT 0,
	sync CHAR(1) NOT NULL DEFAULT "",
	fixed_day VARCHAR(10) NOT NULL DEFAULT "",
	salesarea_id INTEGER NOT NULL DEFAULT 0,
	paycond_id INTEGER NOT NULL DEFAULT 0,
	payment_way TINYINT NOT NULL DEFAULT 0,
	repres_id INTEGER NOT NULL DEFAULT 0,
	pro_dest TINYINT NOT NULL DEFAULT 0,
	KEY pricelist_id (pricelist_id)
	) ENGINE=INNODB DEFAULT CHARSET=UTF8MB4;
-- *************************************************************************!*

-- ***************************** Legal Person ******************************!*
-- @field reg_id = Registry identification
-- @field alias = Alias name
-- @field website = Web address
-- @field crt = Tax System Code: 1 - Simple national; 2 - Simple national, gross revenue sub-limit excess; 3 - Normal regime.
--				Código de Regime Tributário: 1 - Simples Nacional; 2 - Simples Nacional, excesso sublimite de receita bruta; 3 - Regime Normal.
-- @field icms = Percentage of ICMS(Tax on Circulation of Goods and Services) reduction: for exemption, inform 100%
--				 Percentual de redução do ICMS: para isenção informar 100%
-- @field invoiceobs_id = Identification of the invoice observation that will be printed due to the reduction of ICMS
-- @field print_pn = Print PartNumber in code field
-- @field client_id = Client identification
-- @field supplier_id = Supplier identification
-- @field conveyor_id = Conveyor identification
CREATE TABLE IF NOT EXISTS legal_person(
	reg_id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	client_id INTEGER NOT NULL DEFAULT 0,
	alias VARCHAR(50) NOT NULL DEFAULT "",
	website VARCHAR(50) NOT NULL DEFAULT "",
	crt TINYINT NOT NULL DEFAULT 0,
	icms FLOAT NOT NULL DEFAULT 0,
	invoiceobs_id INTEGER NOT NULL DEFAULT 0,
	print_pn TINYINT NOT NULL DEFAULT 0,
	client_id INTEGER NOT NULL DEFAULT 0,
	supplier_id INTEGER NOT NULL DEFAULT 0,
	conveyor_id INTEGER NOT NULL DEFAULT 0,
	KEY alias (alias),
	KEY client_id (client_id),
	KEY supplier_id (supplier_id),
	KEY conveyor_id (conveyor_id),
	CONSTRAINT FK_legal_person_client_id FOREIGN KEY (client_id) REFERENCES client (reg_id) ON UPDATE NO ACTION ON DELETE CASCADE,
	CONSTRAINT FK_legal_person_supplier_id FOREIGN KEY (supplier_id) REFERENCES supplier (reg_id) ON UPDATE NO ACTION ON DELETE CASCADE,
	CONSTRAINT FK_legal_person_conveyor_id FOREIGN KEY (conveyor_id) REFERENCES conveyor (reg_id) ON UPDATE NO ACTION ON DELETE CASCADE
	) ENGINE=INNODB DEFAULT CHARSET=UTF8MB4;
-- *************************************************************************!*

-- **************************** Natural Person *****************************!*
-- @field reg_id = Registry identification
-- @field naturalness = Place of birth
-- @field mar_status = Marital status (Single(S), Married(M), Divorced(D), Widowed(W))
-- @field nationality = Nationality
-- @field gender = Gender ( Masculine(M), Feminine(F) )
-- @field dad_name = Father's name
-- @field mom_name = Mother's name
-- @field rs_since = Reside since
-- @field gross_inc = Gross income
-- @field client_id = Client identification
-- @field supplier_id = Supplier identification
-- @field conveyor_id = Conveyor identification
CREATE TABLE IF NOT EXISTS natural_person(
	reg_id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	client_id INTEGER NOT NULL DEFAULT 0,
	naturalness VARCHAR(50) NOT NULL DEFAULT "",
	mar_status CHAR(1) NOT NULL DEFAULT "",
	nationality VARCHAR(50) NOT NULL DEFAULT "",
	gender CHAR(1) NOT NULL DEFAULT "",
	dad_name VARCHAR(50) NOT NULL DEFAULT "",
	mom_name VARCHAR(50) NOT NULL DEFAULT "",
	rs_since DATE NOT NULL DEFAULT 0,
	gross_inc FLOAT NOT NULL DEFAULT 0,
	client_id INTEGER NOT NULL DEFAULT 0,
	supplier_id INTEGER NOT NULL DEFAULT 0,
	conveyor_id INTEGER NOT NULL DEFAULT 0,
	KEY client_id (client_id),
	KEY supplier_id (supplier_id),
	KEY conveyor_id (conveyor_id),
	CONSTRAINT FK_natural_person_client_id FOREIGN KEY (client_id) REFERENCES client (reg_id) ON UPDATE NO ACTION ON DELETE CASCADE,
	CONSTRAINT FK_natural_person_supplier_id FOREIGN KEY (supplier_id) REFERENCES supplier (reg_id) ON UPDATE NO ACTION ON DELETE CASCADE,
	CONSTRAINT FK_natural_person_conveyor_id FOREIGN KEY (conveyor_id) REFERENCES conveyor (reg_id) ON UPDATE NO ACTION ON DELETE CASCADE
	) ENGINE=INNODB DEFAULT CHARSET=UTF8MB4;
-- *************************************************************************!*

-- ************************* Natural Person Spouse *************************!*
-- @field reg_id = Registry identification
-- @field natural_person_id = Natural Person identification
-- @field spo_name = Spouse's name
-- @field profession = Spouse's profession
-- @field birthday = Spouse's birthday
-- @field income = Spouse's income
-- @field cellphone = Spouse's cell phone
CREATE TABLE IF NOT EXISTS spouse(
	reg_id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	natural_person_id INTEGER NOT NULL DEFAULT 0,
	spo_name VARCHAR(50) NOT NULL DEFAULT "",
	profession VARCHAR(40) NOT NULL DEFAULT "",
	birthday DATE NOT NULL DEFAULT 0,
	income FLOAT NOT NULL DEFAULT 0,
	cellphone VARCHAR(14) NOT NULL DEFAULT "",
	KEY natural_person_id (natural_person_id),
	CONSTRAINT FK_spouse_natural_person_id FOREIGN KEY (natural_person_id) REFERENCES natural_person (reg_id) ON UPDATE NO ACTION ON DELETE CASCADE
	) ENGINE=INNODB DEFAULT CHARSET=UTF8MB4;
-- *************************************************************************!*

-- ******************************** Contact ********************************!*
-- @field reg_id = Registry identification
-- @field phone
-- @field branch_line
-- @field email = E-mail
-- @field contact
-- @field role
-- @field cellphone
-- @field adm_date = Admission date
-- @field client_id = Client identification
-- @field supplier_id = Supplier identification
-- @field conveyor_id = Conveyor identification
CREATE TABLE IF NOT EXISTS contact(
	reg_id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	phone VARCHAR(14) NOT NULL DEFAULT "",
	branch_line VARCHAR(5) NOT NULL DEFAULT "",
	email VARCHAR(50) NOT NULL DEFAULT "",
	contact VARCHAR(30) NOT NULL DEFAULT "",
	role VARCHAR(50) NOT NULL DEFAULT "",
	cellphone VARCHAR(14) NOT NULL DEFAULT "",
	adm_date DATE NOT NULL DEFAULT 0,
	client_id INTEGER NOT NULL DEFAULT 0,
	supplier_id INTEGER NOT NULL DEFAULT 0,
	conveyor_id INTEGER NOT NULL DEFAULT 0,
	KEY contact (contact),
	KEY client_id (client_id),
	KEY supplier_id (supplier_id),
	KEY conveyor_id (conveyor_id),
	CONSTRAINT FK_contact_client_id FOREIGN KEY (client_id) REFERENCES client (reg_id) ON UPDATE NO ACTION ON DELETE CASCADE,
	CONSTRAINT FK_contact_supplier_id FOREIGN KEY (supplier_id) REFERENCES supplier (reg_id) ON UPDATE NO ACTION ON DELETE CASCADE,
	CONSTRAINT FK_contact_conveyor_id FOREIGN KEY (conveyor_id) REFERENCES conveyor (reg_id) ON UPDATE NO ACTION ON DELETE CASCADE
	) ENGINE=INNODB DEFAULT CHARSET=UTF8MB4;
-- *************************************************************************!*

-- ******************************** Account ********************************!*
-- @field reg_id = Registry identification
-- @field bank_id = Bank identification
-- @field branch_code = Bank branch code
-- @field account_numb = Account number
-- @field open_date = Opening date
-- @field client_id = Client identification
-- @field supplier_id = Supplier identification
-- @field conveyor_id = Conveyor identification
CREATE TABLE IF NOT EXISTS account(
	reg_id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	bank_id INTEGER NOT NULL DEFAULT 0,
	branch_code VARCHAR(10) NOT NULL DEFAULT "",
	account_numb VARCHAR(10) NOT NULL DEFAULT "",
	open_date DATE NOT NULL DEFAULT 0,
	client_id INTEGER NOT NULL DEFAULT 0,
	supplier_id INTEGER NOT NULL DEFAULT 0,
	conveyor_id INTEGER NOT NULL DEFAULT 0,
	KEY contact (contact),
	KEY client_id (client_id),
	KEY supplier_id (supplier_id),
	KEY conveyor_id (conveyor_id),
	CONSTRAINT FK_account_client_id FOREIGN KEY (client_id) REFERENCES client (reg_id) ON UPDATE NO ACTION ON DELETE CASCADE,
	CONSTRAINT FK_account_supplier_id FOREIGN KEY (supplier_id) REFERENCES supplier (reg_id) ON UPDATE NO ACTION ON DELETE CASCADE,
	CONSTRAINT FK_account_conveyor_id FOREIGN KEY (conveyor_id) REFERENCES conveyor (reg_id) ON UPDATE NO ACTION ON DELETE CASCADE
	) ENGINE=INNODB DEFAULT CHARSET=UTF8MB4;
-- *************************************************************************!*

-- ******************************** Address ********************************!*
-- @field reg_id = Registry identification
-- @field street = Name of street, avenue, road...
-- @field anumber = Address number
-- @field city
-- @field district
-- @field zipcode
-- @field contact
-- @field phone
-- @field astate = Address state
-- @field complement
-- @field ref_point = Reference point
-- @field atype = Address type can be: 1 - Commercial, 2 - Residential, 3 - Charge, 4 - Delivery
-- @field client_id = Client identification
-- @field supplier_id = Supplier identification
-- @field main = Defines if the address is the main one
CREATE TABLE IF NOT EXISTS address(
	reg_id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	street VARCHAR(60) NOT NULL DEFAULT "",
	anumber VARCHAR(10) NOT NULL DEFAULT "",
	city VARCHAR(50) NOT NULL DEFAULT "",
	district VARCHAR(50) NOT NULL DEFAULT "",
	zipcode VARCHAR(10) NOT NULL DEFAULT "",
	contact VARCHAR(40) NOT NULL DEFAULT "",
	phone VARCHAR(14) NOT NULL DEFAULT "",
	astate VARCHAR(2) NOT NULL DEFAULT "",
	complement VARCHAR(50) NOT NULL DEFAULT "",
	ref_point VARCHAR(50) NOT NULL DEFAULT "",
	atype TINYINT NOT NULL DEFAULT 0,
	main BOOL NOT NULL DEFAULT 0,
	client_id INTEGER NOT NULL DEFAULT 0,
	supplier_id INTEGER NOT NULL DEFAULT 0,
	conveyor_id INTEGER NOT NULL DEFAULT 0,
	KEY client_id (client_id),
	KEY supplier_id (supplier_id),
	KEY conveyor_id (conveyor_id),
	CONSTRAINT FK_address_client_id FOREIGN KEY (client_id) REFERENCES client (reg_id) ON UPDATE NO ACTION ON DELETE CASCADE,
	CONSTRAINT FK_address_supplier_id FOREIGN KEY (supplier_id) REFERENCES supplier (reg_id) ON UPDATE NO ACTION ON DELETE CASCADE,
	CONSTRAINT FK_address_conveyor_id FOREIGN KEY (conveyor_id) REFERENCES conveyor (reg_id) ON UPDATE NO ACTION ON DELETE CASCADE
	) ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4

-- ******************************* Messenger *******************************!*
-- @field reg_id = Registry identification
-- @field client_id = Client identification
-- @field type_chat = Type of chat
-- @field chat_id = Chat identification
CREATE TABLE IF NOT EXISTS messenger(
	reg_id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	type_chat VARCHAR(20) NOT NULL DEFAULT "",
	chat_id VARCHAR(50) NOT NULL DEFAULT "",
	client_id INTEGER NOT NULL DEFAULT 0,
	supplier_id INTEGER NOT NULL DEFAULT 0,
	conveyor_id INTEGER NOT NULL DEFAULT 0,
	KEY client_id (client_id),
	KEY supplier_id (supplier_id),
	KEY conveyor_id (conveyor_id),
	CONSTRAINT FK_messenger_client_id FOREIGN KEY (client_id) REFERENCES client (reg_id) ON UPDATE NO ACTION ON DELETE CASCADE,
	CONSTRAINT FK_messenger_supplier_id FOREIGN KEY (supplier_id) REFERENCES supplier (reg_id) ON UPDATE NO ACTION ON DELETE CASCADE,
	CONSTRAINT FK_messenger_conveyor_id FOREIGN KEY (conveyor_id) REFERENCES conveyor (reg_id) ON UPDATE NO ACTION ON DELETE CASCADE
	) ENGINE=INNODB DEFAULT CHARSET=UTF8MB4;
-- *************************************************************************!*
