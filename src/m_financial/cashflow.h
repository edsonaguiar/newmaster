#ifndef CASHFLOW_H
#define CASHFLOW_H

#include <QWidget>

namespace Ui {
class PosClosing;
}

class PosClosing : public QWidget
{
    Q_OBJECT

public:
    explicit PosClosing(QWidget *parent = 0);
    ~PosClosing();

private:
    Ui::PosClosing *ui;
};

#endif // CASHFLOW_H
