#include "db_conn.h"
#include "database.h"
#include <QSqlDatabase>

Database::Database(){
    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName(DataConnection::getHost());
    db.setDatabaseName(DataConnection::getName());
    db.setUserName(DataConnection::getUser());
    db.setPassword(DataConnection::getPassword());
    connected = db.open();
}
