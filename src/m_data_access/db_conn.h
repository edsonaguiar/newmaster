#ifndef DB_CONN_H
#define DB_CONN_H
#include <QString>

class DataConnection
{
private:
    static QString db_user;
    static QString db_password;
    static QString db_host;
    static QString db_name;
public:
    static void setUser(QString dbuser);
    static QString getUser();

    static void setPassword(QString dbpassword);
    static QString getPassword();

    static void setHost(QString dbhost);
    static QString getHost();

    static void setName(QString dbname);
    static QString getName();
};

#endif // DB_CONN_H
