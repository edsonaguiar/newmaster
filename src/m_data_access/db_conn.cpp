#include "db_conn.h"

QString DataConnection::db_user;
QString DataConnection::db_password;
QString DataConnection::db_host;
QString DataConnection::db_name;

void DataConnection::setUser(QString dbuser) {db_user = dbuser;}
QString DataConnection::getUser() {return db_user;}

void DataConnection::setPassword(QString dbpassword) {db_password = dbpassword;}
QString DataConnection::getPassword() {return db_password;}

void DataConnection::setHost(QString dbhost) {db_host = dbhost;}
QString DataConnection::getHost() {return db_host;}

void DataConnection::setName(QString dbname) {db_name = dbname;}
QString DataConnection::getName() {return db_name;}
