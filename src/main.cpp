#include "m_main/desktop.h"
#include <QApplication>
#include <QMessageBox>

using namespace std;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QTranslator tr;
    //QString lang {"en_US"};
    QString lang {"pt_BR"};
    bool loadOK = true;
    if(lang == "pt_BR"){
        loadOK = tr.load(qApp->applicationDirPath() + "/portuguese.qm");
    }
    if(!loadOK){
        QMessageBox::warning(0,"Language Error","An error has ocurred while loading Portuguese language.", QMessageBox::Ok);
    }
    a.installTranslator(&tr);
    a.setStyle("fusion");
    Desktop w;
    w.showMaximized();

    return a.exec();
}
