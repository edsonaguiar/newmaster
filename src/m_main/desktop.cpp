#include "desktop.h"
#include "ui_desktop.h"

//#include "m_main/decvar.h"
#include "m_registry/client.h"
//#include "m_registry/product.h"

Desktop::Desktop(QWidget *parent) : QMainWindow(parent), ui(new Ui::Desktop)
{
    ui->setupUi(this);
    setCentralWidget(ui->mdiArea);
    delete ui->menuFinancial;
    delete ui->menuHelp;
    delete ui->menuPurchases;
    delete ui->menuReports;
    delete ui->menuSales;
    delete ui->menuTools;
    delete ui->menuWindows;
    delete ui->menuRegistry_Employees;
    delete ui->menuRegistry_Materials;
    delete ui->menuRegistry_Finishers;
    delete ui->menuRegistry_RegistrySecurity;
    delete ui->actionRegistry_ProspectClients;
    delete ui->actionRegistry_ProspectControl;
    delete ui->actionRegistry_Suppliers;
    delete ui->actionRegistry_Conveyors;
    delete ui->actionRegistry_Banks;
    delete ui->actionRegistry_BankAgencies;
    delete ui->actionRegistry_Accounts;
    delete ui->actionRegistry_CostCenters;
    delete ui->actionRegistry_BudgetAccounts;
    delete ui->actionRegistry_DocumentsTypes;
    delete ui->actionRegistry_CFOPs;
    delete ui->actionRegistry_NotesOnTheInvoice;
    delete ui->actionRegistry_PaymentConditions;
    delete ui->actionRegistry_AccountsPlan;
    delete ui->actionRegistry_SalesArea;
}

Desktop::~Desktop()
{
    delete ui;
}

void Desktop::loadSubWindow(QMainWindow *widget)
{
    QMdiArea *mdi = this->ui->mdiArea;
    QMdiSubWindow *window = mdi->addSubWindow(widget);
    window->setGeometry(
        (mdi->width() - widget->geometry().width()) / 2,
        (mdi->height() - widget->geometry().height()) / 2,
        widget->width(), widget->height()
    );
    window->show();
}

void Desktop::on_actionRegistry_Clients_triggered()
{
    loadSubWindow(new Client(this));
}


void Desktop::on_actionRegistry_Materials_Products_triggered()
{
    //loadSubWindow(new Product(this));
}

