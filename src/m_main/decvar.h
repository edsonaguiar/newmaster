#ifndef DECVAR_H
#define DECVAR_H
#include <QString>

using namespace std;

extern string usu;

class DeclareVariables
{
private:
    const QString KEY1 = "_19021722";
    int user_id = 0;
    QString user_name = "";
    bool adm_user = true;
    QString sys_path = "";
    int computer_id = 0;
public:
    DeclareVariables();
    QString getKey() {return this->KEY1;}

    void setUserId(int userid) {this->user_id = userid;}
    int getIdUsuario() {return this->user_id;}

    void setUserName(QString username) {this->user_name = username;}
    QString getUserName() {return this->user_name;}

    void setAdmUser(bool admuser) {this->adm_user = admuser;}
    bool getAdmUser() {return this->adm_user;}

    void setSysPath(QString syspath) {this->sys_path = syspath;}
    QString getSysPath() {return this->sys_path;}

    void setComputerId(int compid) {this->computer_id = compid;}
    int getComputerId() {return this->computer_id;}
};

#endif // DECVAR_H
