#ifndef DESKTOP_H
#define DESKTOP_H

#include <QTranslator>
#include <QMainWindow>
#include <QMdiSubWindow>
#include <QMessageBox>

#include "decvar.h"

namespace Ui {
class Desktop;
}

class Desktop : public QMainWindow
{
    Q_OBJECT

public:
    explicit Desktop(QWidget *parent = 0);
    ~Desktop();
    DeclareVariables var;

private slots:
    void loadSubWindow(QMainWindow *widget);
    void on_actionRegistry_Clients_triggered();
    void on_actionRegistry_Materials_Products_triggered();

private:
    Ui::Desktop *ui;
};

#endif // DESKTOP_H
