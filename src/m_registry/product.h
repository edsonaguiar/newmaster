#ifndef PRODUCT_H
#define PRODUCT_H

#include <QWidget>

namespace Ui {
class Product;
}

class Product : public QWidget
{
    Q_OBJECT

public:
    explicit Product(QWidget *parent = 0);
    ~Product();

private slots:
    void enable(bool enab);
    void on_btnChange_clicked();
    void on_btnInclude_clicked();

private:
    Ui::Product *ui;
};

#endif // PRODUCT_H
