#include "client.h"
#include "ui_client.h"

Client::Client(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Client)
{
    ui->setupUi(this);
    ui->tbwAddress->setColumnWidth(0, 40);
    ui->tbwAddress->setColumnWidth(1, 60);
    ui->tbwAddress->setColumnWidth(2, 200);
    ui->tbwAddress->setColumnWidth(3, 60);
    ui->tbwAddress->setColumnWidth(4, 100);
    ui->tbwAddress->setColumnWidth(5, 100);
    ui->tbwAddress->setColumnWidth(6, 150);
    ui->tbwAddress->setColumnWidth(7, 30);
    ui->tbwAddress->setColumnWidth(8, 110);
    ui->tbwAddress->setColumnWidth(9, 100);
    ui->tbwAddress->setColumnWidth(10, 120);
    ui->tbwAddress->setColumnWidth(11, 100);

    ui->cbbType->addItem(tr("Legal Person"), 0);
    ui->cbbType->addItem(tr("Natural Person"), 1);
    ui->cbbType->setCurrentIndex(0);

    ui->cbbSituation->addItem(tr("Active"), 0);
    ui->cbbSituation->addItem(tr("Inactive"), 1);
    ui->cbbSituation->addItem(tr("Bloqued"), 2);
    ui->cbbSituation->setCurrentIndex(0);

    ui->cbbTaxRegimeCode->addItem(tr("1 - Simple national"), 1);
    ui->cbbTaxRegimeCode->addItem(tr("2 - Simple national, gross revenue sub-limit excess"), 2);
    ui->cbbTaxRegimeCode->addItem(tr("3 - Normal regime"), 3);
    ui->cbbTaxRegimeCode->setCurrentIndex(0);

    ui->cbbDestination->addItem(tr("Comsumption"), 0);
    ui->cbbDestination->addItem(tr("Resale"), 1);
    ui->cbbDestination->addItem(tr("Industrialization"), 2);
    ui->cbbDestination->setCurrentIndex(0);

    ui->cbbContactMessaging->addItem("WHATSAPP", 0);
    ui->cbbContactMessaging->addItem("TELEGRAM", 1);
    ui->cbbContactMessaging->addItem("SKYPE", 2);
    ui->cbbContactMessaging->addItem("FACEBOOK", 3);
    ui->cbbContactMessaging->addItem("YAHOO", 4);
    ui->cbbContactMessaging->addItem("GOOGLE TALK", 5);
    ui->cbbContactMessaging->addItem("ICQ", 6);
    ui->cbbContactMessaging->addItem("AIM", 7);
    ui->cbbContactMessaging->addItem("JABBER", 8);
    ui->cbbContactMessaging->addItem("IRQ", 9);
    ui->cbbContactMessaging->addItem("MSN", 10);
    ui->cbbContactMessaging->setCurrentIndex(0);

    ui->cbbPaymentMethod->addItem(tr("None"), 0);
    ui->cbbPaymentMethod->addItem(tr("Money"), 1);
    ui->cbbPaymentMethod->addItem(tr("Card"), 2);
    ui->cbbPaymentMethod->addItem(tr("Check"), 3);
    ui->cbbPaymentMethod->addItem(tr("Bank Slip"), 4);
    ui->cbbPaymentMethod->addItem(tr("Installment Plan"), 5);
    ui->cbbPaymentMethod->addItem(tr("Deposit"), 6);
    ui->cbbPaymentMethod->setCurrentIndex(0);

    ui->cbbMaritalStatus->addItem(tr("Single"), 0);
    ui->cbbMaritalStatus->addItem(tr("Married"), 1);
    ui->cbbMaritalStatus->addItem(tr("Divorced"), 2);
    ui->cbbMaritalStatus->addItem(tr("Widowed"), 3);
    ui->cbbMaritalStatus->setCurrentIndex(0);

    ui->cbbGender->addItem(tr("Male"), 0);
    ui->cbbGender->addItem(tr("Female"), 1);
    ui->cbbGender->setCurrentIndex(0);

    ui->cbbRefType1->addItem(tr("Commercial"), 0);
    ui->cbbRefType1->addItem(tr("Personal"), 1);
    ui->cbbRefType1->setCurrentIndex(-1);
    ui->cbbRefType2->addItem(tr("Commercial"), 0);
    ui->cbbRefType2->addItem(tr("Personal"), 1);
    ui->cbbRefType2->setCurrentIndex(-1);
    ui->cbbRefType3->addItem(tr("Commercial"), 0);
    ui->cbbRefType3->addItem(tr("Personal"), 1);
    ui->cbbRefType3->setCurrentIndex(-1);
}

Client::~Client()
{
    delete ui;
}
