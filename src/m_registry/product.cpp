#include "product.h"
#include "ui_product.h"

Product::Product(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Product)
{
    ui->setupUi(this);
}

Product::~Product()
{
    delete ui;
}

void Product::enable(bool enab)
{
    ui->btnClose->setVisible(enab);
    ui->btnDelete->setVisible(enab);
    ui->btnFind->setVisible(enab);
    if (enab) {
        ui->btnChange->setText(tr("&Change"));
        ui->btnInclude->setText(tr("&Include"));
    } else {
        ui->btnChange->setText(tr("&Cancel"));
        ui->btnInclude->setText(tr("&Save"));
    }
    ui->edtCode->setEnabled(enab);
    ui->edtDescription->setEnabled(not enab);
    ui->edtFactory->setEnabled(not enab);
    ui->edtPrice->setEnabled(not enab);
}
void Product::on_btnChange_clicked()
{
    enable(true);
}

void Product::on_btnInclude_clicked()
{
    enable(false);
}
