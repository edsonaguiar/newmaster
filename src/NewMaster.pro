#-------------------------------------------------
#
# Project created by QtCreator 2017-04-15T18:09:44
#
#-------------------------------------------------

QT      += core gui
QT      += sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

TARGET = NewMaster
TEMPLATE = app


SOURCES += main.cpp\
    m_financial/cashflow.cpp \
    m_main/decvar.cpp \
    m_main/desktop.cpp \
    m_registry/product.cpp \
    m_main/start.cpp \
    m_registry/client.cpp \
    m_data_access/database.cpp \
    m_data_access/db_conn.cpp

HEADERS  += \
    m_financial/cashflow.h \
    m_main/decvar.h \
    m_main/desktop.h \
    m_registry/product.h \
    m_main/start.h \
    m_registry/client.h \
    m_data_access/database.h \
    m_data_access/db_conn.h

FORMS    += \
    m_financial/cashflow.ui \
    m_main/desktop.ui \
    m_registry/product.ui \
    m_registry/client.ui

RESOURCES += \
	resource.qrc
