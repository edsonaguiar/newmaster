<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR" sourcelanguage="en_US">
<context>
    <name>Client</name>
    <message>
        <location filename="m_registry/client.ui" line="14"/>
        <source>Clients Registry</source>
        <translation>Cadastro de Clientes</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="34"/>
        <source>Code</source>
        <translation>Código</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="51"/>
        <source>Alias</source>
        <translation>Fantasia</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="88"/>
        <source>Identification</source>
        <translation>Identificação</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="98"/>
        <source>Type:</source>
        <translation>Tipo:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="108"/>
        <source>Situation:</source>
        <translation>Situação:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="138"/>
        <source>Company name:</source>
        <translation>Razão social:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="152"/>
        <source>Federal registry:</source>
        <translation>CNPJ:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="162"/>
        <source>State registry:</source>
        <translation>Inscrição estadual:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="182"/>
        <source>Place of birth:</source>
        <translation>Naturalidade:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="192"/>
        <source>Registry date:</source>
        <translation>Data de registro:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="219"/>
        <source>Birth date:</source>
        <translation>Data de nascimento:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="236"/>
        <source>Marital status:</source>
        <translation>Estado civil:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="263"/>
        <source>Father&apos;s name:</source>
        <translation>Nome do pai:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="277"/>
        <source>Mother&apos;s name:</source>
        <translation>Nome da mãe:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="329"/>
        <source>&amp;Insert</source>
        <translation>&amp;Adicionar</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="336"/>
        <location filename="m_registry/client.ui" line="2008"/>
        <source>&amp;Edit</source>
        <translation>&amp;Editar</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="343"/>
        <location filename="m_registry/client.ui" line="2026"/>
        <source>&amp;Delete</source>
        <translation>&amp;Excluir</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="350"/>
        <source>&amp;Main</source>
        <translation>&amp;Principal</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="381"/>
        <source>Main</source>
        <translation>Pcpal</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="386"/>
        <location filename="m_registry/client.ui" line="1220"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="391"/>
        <source>Address</source>
        <translation>Endereço</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="396"/>
        <source>Number</source>
        <translation>Número</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="401"/>
        <source>Complement</source>
        <translation>Complemento</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="406"/>
        <source>Ref. Point</source>
        <translation>Ponto de ref</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="411"/>
        <source>City</source>
        <translation>Cidade</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="416"/>
        <source>State</source>
        <translation>Estado</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="421"/>
        <source>District</source>
        <translation>Bairro</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="426"/>
        <source>Zip Code</source>
        <translation>CEP</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="209"/>
        <source>Nationality:</source>
        <translation>Nacionalidade:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="246"/>
        <source>Gender:</source>
        <translation>Sexo:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="291"/>
        <source>Website:</source>
        <translation>Website:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="319"/>
        <source>Addresses</source>
        <translation>Endereço</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="431"/>
        <location filename="m_registry/client.ui" line="449"/>
        <source>Contact</source>
        <translation>Contato</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="436"/>
        <location filename="m_registry/client.ui" line="1207"/>
        <source>Phone</source>
        <translation>Telefone</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="459"/>
        <location filename="m_registry/client.ui" line="1245"/>
        <location filename="m_registry/client.ui" line="1874"/>
        <source>Name:</source>
        <translation>Nome:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="469"/>
        <source>Admission date:</source>
        <translation>Data de admissão:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="486"/>
        <source>Role:</source>
        <translation>Cargo:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="496"/>
        <source>Cellphone:</source>
        <translation>Celular:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="513"/>
        <location filename="m_registry/client.ui" line="791"/>
        <location filename="m_registry/client.ui" line="821"/>
        <location filename="m_registry/client.ui" line="1840"/>
        <source>Phone:</source>
        <translation>Telefone:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="523"/>
        <source>Branch line:</source>
        <translation>Ramal:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="536"/>
        <source>Phone 2:</source>
        <translation>Telefone 2:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="553"/>
        <source>Messaging:</source>
        <translation>Mensageiro:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="582"/>
        <location filename="m_registry/client.ui" line="594"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="622"/>
        <source>E-mail:</source>
        <translation>E-mail:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="636"/>
        <source>E-mail invoice:</source>
        <translation>E-mail NFe:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="657"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:7pt; color:#ff0000;&quot;&gt;**To enter more than one e-mail, separate them by semicolon.&lt;br&gt;(Ex.: email1@dominioemail1.com.br; email2@dominioemail2.com.br; email3@dominioemail3.com.br)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:7pt; color:#ff0000;&quot;&gt;**Para informar mais de um email, separá-los utilizando ponto e vírgula.&lt;br&gt;(Ex.: email1@dominioemail1.com.br; email2@dominioemail2.com.br; email3@dominioemail3.com.br)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="669"/>
        <source>Charging</source>
        <translation>Cobrança</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="684"/>
        <source>Financial:</source>
        <translation>Financeiro:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="708"/>
        <source>Bank:</source>
        <translation>Banco:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="732"/>
        <source>Agencie:</source>
        <translation>Agência:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="742"/>
        <source>Account:</source>
        <translation>Conta:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="758"/>
        <source>Opening date:</source>
        <translation>Data de abertura:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="778"/>
        <source>Commercial responsible:</source>
        <translation>Responsável comercial:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="808"/>
        <source>Financial responsible:</source>
        <translation>Responsável financeiro:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="851"/>
        <source>Day</source>
        <translation>Dia</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="865"/>
        <source>Fixed billing date:</source>
        <translation>Data fixa para pagamento:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="891"/>
        <location filename="m_registry/client.ui" line="904"/>
        <location filename="m_registry/client.ui" line="917"/>
        <location filename="m_registry/client.ui" line="930"/>
        <source>  or  </source>
        <translation>  ou  </translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="974"/>
        <source>Credit</source>
        <translation>Crédito</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="986"/>
        <source>Credit for new purchases:</source>
        <translation>Crédito para novas compras:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1003"/>
        <source>Last purchase:</source>
        <translation>Última compra:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1018"/>
        <source>Information:</source>
        <translation>Informação:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1038"/>
        <source>Gross income:</source>
        <translation>Receita bruta:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1079"/>
        <source>Included in the Video Check</source>
        <translation>Incluso no Video Cheque</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1086"/>
        <source>Included in the SPC</source>
        <translation>Incluso no SPC</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1093"/>
        <source>Included in the Serasa</source>
        <translation>Incluso no Serasa</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1100"/>
        <source>Have outstanding debts</source>
        <translation>Possui débitos em atraso</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1107"/>
        <source>Pending documentation</source>
        <translation>Documentação pendente</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1144"/>
        <source>Available</source>
        <translation>Disponível</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1171"/>
        <source>Total</source>
        <translation>Total</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1181"/>
        <source>Purchase limit:</source>
        <translation>Limite de compras:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1232"/>
        <source>References:</source>
        <translation>Referências:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1285"/>
        <source>Invoicing</source>
        <translation>Faturamento</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1304"/>
        <source>Conveyor:</source>
        <translation>Transportador:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1340"/>
        <source>Payment method:</source>
        <translation>Forma de pagamento:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1363"/>
        <source>Seller:</source>
        <translation>Vendedor:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1399"/>
        <source>Destination:</source>
        <translation>Destinação:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1422"/>
        <source>External seller:</source>
        <translation>Vendedor externo:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1458"/>
        <source>Tax regime code:</source>
        <translation>CRT:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1481"/>
        <source>Representative:</source>
        <translation>Representante:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1517"/>
        <source>Markup:</source>
        <translation>Margem de lucro:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1540"/>
        <source>Price table:</source>
        <translation>Tabela de preços:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1576"/>
        <source>Updated:</source>
        <translation>Atualizado em:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1599"/>
        <source>Sells area:</source>
        <translation>Área de vendas:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1635"/>
        <source>Sales tax:</source>
        <translation>% ICMS:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1658"/>
        <source>Payment condition:</source>
        <translation>Condição de pagto:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1694"/>
        <source>Sales tax reduction:</source>
        <translation>% Redução ICMS:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1720"/>
        <source>Invoice observation:</source>
        <translation>Observações NFe:</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1754"/>
        <source>Print part number in the invoice code field</source>
        <oldsource>Print part number in the invoice code field:</oldsource>
        <translation>Imprimir PartNumber no campo código da NF</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1761"/>
        <source>Does not calculate difference in sales tax</source>
        <translation>Não calcula diferença de ICMS</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1775"/>
        <source>People authorized to purchase</source>
        <translation>Pessoas autorizadas para compra</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1867"/>
        <source>Itin</source>
        <translation>CPF</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1906"/>
        <location filename="m_registry/client.ui" line="1914"/>
        <source>Observation</source>
        <translation>Observação</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1946"/>
        <source>&amp;Movement</source>
        <translation>&amp;Movimento</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1953"/>
        <source>&amp;Change</source>
        <translation>&amp;Alterar</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1973"/>
        <source>toolBar</source>
        <translation>toolBar</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="1999"/>
        <source>&amp;New</source>
        <translation>&amp;Novo</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="2017"/>
        <source>&amp;Find</source>
        <translation>&amp;Localizar</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="2035"/>
        <source>Fi&amp;rst</source>
        <translation>&amp;Primeiro</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="2044"/>
        <source>&amp;Prior</source>
        <translation>&amp;Anterior</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="2053"/>
        <source>Nex&amp;t</source>
        <translation>Pró&amp;ximo</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="2062"/>
        <source>&amp;Last</source>
        <translation>&amp;Último</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="2067"/>
        <source>&amp;Purchases</source>
        <translation>&amp;Compras</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="2072"/>
        <source>&amp;Items</source>
        <translation>&amp;Itens</translation>
    </message>
    <message>
        <location filename="m_registry/client.ui" line="2081"/>
        <source>E&amp;xit</source>
        <translation>&amp;Sair</translation>
    </message>
    <message>
        <location filename="m_registry/client.cpp" line="22"/>
        <source>Legal Person</source>
        <translation>Jurídica</translation>
    </message>
    <message>
        <location filename="m_registry/client.cpp" line="23"/>
        <source>Natural Person</source>
        <translation>Física</translation>
    </message>
    <message>
        <location filename="m_registry/client.cpp" line="26"/>
        <source>Active</source>
        <translation>Ativo</translation>
    </message>
    <message>
        <location filename="m_registry/client.cpp" line="27"/>
        <source>Inactive</source>
        <translation>Inativo</translation>
    </message>
    <message>
        <location filename="m_registry/client.cpp" line="28"/>
        <source>Bloqued</source>
        <translation>Bloqueado</translation>
    </message>
    <message>
        <location filename="m_registry/client.cpp" line="31"/>
        <source>1 - Simple national</source>
        <translation>1 - Simples Nacional</translation>
    </message>
    <message>
        <location filename="m_registry/client.cpp" line="32"/>
        <source>2 - Simple national, gross revenue sub-limit excess</source>
        <translation>2 - Simples Nacional - excesso de sublimite da receita bruta</translation>
    </message>
    <message>
        <location filename="m_registry/client.cpp" line="33"/>
        <source>3 - Normal regime</source>
        <translation>3 - Regime Normal</translation>
    </message>
    <message>
        <location filename="m_registry/client.cpp" line="36"/>
        <source>Comsumption</source>
        <translation>Consumo</translation>
    </message>
    <message>
        <location filename="m_registry/client.cpp" line="37"/>
        <source>Resale</source>
        <translation>Revenda</translation>
    </message>
    <message>
        <location filename="m_registry/client.cpp" line="38"/>
        <source>Industrialization</source>
        <translation>Industrialização</translation>
    </message>
    <message>
        <location filename="m_registry/client.cpp" line="54"/>
        <source>None</source>
        <translation>Nenhum</translation>
    </message>
    <message>
        <location filename="m_registry/client.cpp" line="55"/>
        <source>Money</source>
        <translation>Dinheiro</translation>
    </message>
    <message>
        <location filename="m_registry/client.cpp" line="56"/>
        <source>Card</source>
        <translation>Cartão</translation>
    </message>
    <message>
        <location filename="m_registry/client.cpp" line="57"/>
        <source>Check</source>
        <translation>Cheque</translation>
    </message>
    <message>
        <location filename="m_registry/client.cpp" line="58"/>
        <source>Bank Slip</source>
        <translation>Boleto</translation>
    </message>
    <message>
        <location filename="m_registry/client.cpp" line="59"/>
        <source>Installment Plan</source>
        <translation>Crediário</translation>
    </message>
    <message>
        <location filename="m_registry/client.cpp" line="60"/>
        <source>Deposit</source>
        <translation>Depósito</translation>
    </message>
    <message>
        <location filename="m_registry/client.cpp" line="63"/>
        <source>Single</source>
        <oldsource>Sinlge</oldsource>
        <translation>Solteiro(a)</translation>
    </message>
    <message>
        <location filename="m_registry/client.cpp" line="64"/>
        <source>Married</source>
        <translation>Casado(a)</translation>
    </message>
    <message>
        <location filename="m_registry/client.cpp" line="65"/>
        <source>Divorced</source>
        <translation>Divorciado(a)</translation>
    </message>
    <message>
        <location filename="m_registry/client.cpp" line="66"/>
        <source>Widowed</source>
        <translation>Viúvo(a)</translation>
    </message>
    <message>
        <location filename="m_registry/client.cpp" line="69"/>
        <source>Male</source>
        <translation>Masculino</translation>
    </message>
    <message>
        <location filename="m_registry/client.cpp" line="70"/>
        <source>Female</source>
        <translation>Feminino</translation>
    </message>
    <message>
        <location filename="m_registry/client.cpp" line="73"/>
        <location filename="m_registry/client.cpp" line="76"/>
        <location filename="m_registry/client.cpp" line="79"/>
        <source>Commercial</source>
        <translation>Comercial</translation>
    </message>
    <message>
        <location filename="m_registry/client.cpp" line="74"/>
        <location filename="m_registry/client.cpp" line="77"/>
        <location filename="m_registry/client.cpp" line="80"/>
        <source>Personal</source>
        <translation>Pessoal</translation>
    </message>
</context>
<context>
    <name>Desktop</name>
    <message>
        <location filename="m_main/desktop.ui" line="14"/>
        <source>NewMaster</source>
        <translation>NewMaster</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="39"/>
        <location filename="m_main/desktop.ui" line="198"/>
        <location filename="m_main/desktop.ui" line="838"/>
        <source>&amp;Registry</source>
        <translation>&amp;Cadastro</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="43"/>
        <source>&amp;Employees</source>
        <translation>F&amp;uncionários</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="51"/>
        <source>&amp;Materials</source>
        <translation>&amp;Materiais</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="62"/>
        <source>Securit&amp;y</source>
        <translation>Se&amp;gurança</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="69"/>
        <source>Finis&amp;hers</source>
        <translation>F&amp;inalizadoras</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="100"/>
        <source>&amp;Sales</source>
        <translation>&amp;Vendas</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="104"/>
        <source>&amp;Retail</source>
        <translation>&amp;Varejo</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="115"/>
        <source>&amp;Wholesale</source>
        <translation>&amp;Atacado</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="129"/>
        <location filename="m_main/desktop.ui" line="206"/>
        <location filename="m_main/desktop.ui" line="645"/>
        <source>&amp;Purchases</source>
        <translation>Co&amp;mpras</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="140"/>
        <location filename="m_main/desktop.ui" line="213"/>
        <source>&amp;Financial</source>
        <translation>&amp;Financeiro</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="144"/>
        <source>Bills to &amp;Receive</source>
        <translation>Contas a &amp;Receber</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="153"/>
        <source>Bills to &amp;Pay</source>
        <translation>Contas a &amp;Pagar</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="159"/>
        <source>&amp;Account Entries</source>
        <translation>Lançamentos de C&amp;ontas</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="169"/>
        <source>&amp;Bank Entries</source>
        <translation>Lançamentos &amp;Bancários</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="187"/>
        <source>R&amp;eports</source>
        <translation>&amp;Relatórios</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="191"/>
        <source>&amp;Stock</source>
        <translation>&amp;Estoque</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="225"/>
        <source>S&amp;ales</source>
        <translation>&amp;Vendas</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="244"/>
        <source>&amp;Tools</source>
        <translation>F&amp;erramentas</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="264"/>
        <source>&amp;Windows</source>
        <translation>&amp;Janelas</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="273"/>
        <source>&amp;Help</source>
        <translation>Aj&amp;uda</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="310"/>
        <source>Prospect C&amp;lients</source>
        <translation>Clientes P&amp;rospecto</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="315"/>
        <source>Prospect C&amp;ontrol</source>
        <translation>Controle &amp;de Prospectos</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="320"/>
        <source>&amp;Suppliers</source>
        <translation>&amp;Fornecedores</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="325"/>
        <source>&amp;Sellers</source>
        <translation>&amp;Vendedores</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="330"/>
        <source>&amp;Buyers</source>
        <translation>&amp;Compradores</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="335"/>
        <source>&amp;Collaborators</source>
        <translation>C&amp;olaboradores</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="340"/>
        <source>Con&amp;veyors</source>
        <translation>&amp;Transportadores</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="350"/>
        <source>Price &amp;Table</source>
        <translation>&amp;Tabelas de Preço</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="355"/>
        <source>&amp;Groups</source>
        <translation>&amp;Grupos</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="360"/>
        <source>&amp;Subgroups</source>
        <translation>&amp;Subgrupos</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="365"/>
        <source>&amp;Manufacturers</source>
        <translation>&amp;Fabricantes</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="370"/>
        <source>&amp;NCMs</source>
        <translation>&amp;NCMs</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="375"/>
        <source>&amp;Banks</source>
        <translation>&amp;Bancos</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="380"/>
        <source>Ban&amp;k Agencies</source>
        <translation>&amp;Agências Bancárias</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="385"/>
        <source>&amp;Accounts</source>
        <translation>C&amp;ontas</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="390"/>
        <source>Cos&amp;t Centers</source>
        <translation>C&amp;entros de Custo</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="395"/>
        <source>Bud&amp;get Accounts</source>
        <translation>Co&amp;ntas Orçamentárias</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="400"/>
        <source>&amp;Documents Types</source>
        <translation>Tipos de &amp;Documentos</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="405"/>
        <source>C&amp;FOPs</source>
        <translation>CFO&amp;Ps</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="410"/>
        <source>&amp;Notes on the Invoice</source>
        <translation>Ob&amp;servações da Nota Fiscal</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="415"/>
        <source>&amp;Payment Conditions</source>
        <translation>Condi&amp;ções de Pagamento</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="420"/>
        <source>Acco&amp;unts Plan</source>
        <translation>P&amp;lano de Contas</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="425"/>
        <source>Sales A&amp;rea</source>
        <translation>Áreas de &amp;Venda</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="430"/>
        <source>&amp;Users</source>
        <translation>&amp;Usuários</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="435"/>
        <source>Users &amp;Groups</source>
        <translation>&amp;Grupos de Usuários</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="440"/>
        <source>&amp;Credit Cards</source>
        <translation>&amp;Cartões de Crédito</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="445"/>
        <source>Credit Cards &amp;Administrators</source>
        <translation>&amp;Administradoras de Cartões de Crédito</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="450"/>
        <source>&amp;Product Entry</source>
        <translation>&amp;Entrada de Produtos</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="455"/>
        <source>Purchases &amp;Control</source>
        <translation>&amp;Controle de Compras</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="460"/>
        <source>&amp;Update Product Registry</source>
        <translation>&amp;Atualizar Cadastro de Produtos</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="465"/>
        <location filename="m_main/desktop.ui" line="650"/>
        <source>Purchase &amp;Orders</source>
        <translation>&amp;Ordens de Compra</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="470"/>
        <source>Pu&amp;rchase Orders Control</source>
        <translation>Co&amp;ntrole de Ordens de Compra</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="475"/>
        <source>&amp;Quotation</source>
        <translation>Co&amp;tações</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="480"/>
        <location filename="m_main/desktop.ui" line="505"/>
        <location filename="m_main/desktop.ui" line="660"/>
        <source>&amp;Credit</source>
        <translation>&amp;Crédito</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="485"/>
        <source>&amp;Card</source>
        <translation>C&amp;artão</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="490"/>
        <source>&amp;Installment Plan</source>
        <translation>C&amp;rediário</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="495"/>
        <location filename="m_main/desktop.ui" line="540"/>
        <source>&amp;Bank Slip</source>
        <translation>&amp;Boleto</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="500"/>
        <location filename="m_main/desktop.ui" line="510"/>
        <location filename="m_main/desktop.ui" line="665"/>
        <source>&amp;Debit</source>
        <translation>&amp;Débito</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="515"/>
        <source>&amp;Transfer between Accounts</source>
        <translation>&amp;Transferência entre Contas</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="520"/>
        <source>Transfer &amp;Movement</source>
        <translation>&amp;Movimento de Transferências</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="525"/>
        <source>C&amp;redit to Clients</source>
        <translation>C&amp;rédito para Clientes</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="530"/>
        <source>&amp;Checks</source>
        <translation>&amp;Cheques</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="535"/>
        <source>C&amp;hecks Control</source>
        <translation>C&amp;ontrole de Cheques</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="545"/>
        <source>&amp;Generate Bank Slip Remittance</source>
        <translation>&amp;Gerar Remessa de Boletos</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="550"/>
        <source>Bank &amp;Slip Remittance Return</source>
        <translation>&amp;Retorno de Remessa de Boletos</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="555"/>
        <source>&amp;Daily Closing</source>
        <translation>&amp;Fechamento Diário</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="560"/>
        <source>&amp;Point of Sales</source>
        <translation>&amp;Frente de Caixa</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="565"/>
        <location filename="m_main/desktop.ui" line="600"/>
        <source>&amp;Budgets</source>
        <translation>&amp;Orçamentos</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="570"/>
        <source>Pre-&amp;Sales</source>
        <translation>&amp;Pré-Vendas</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="575"/>
        <source>Pre-Sales &amp;Control</source>
        <translation>&amp;Controle de Pré-Vendas</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="580"/>
        <location filename="m_main/desktop.ui" line="610"/>
        <source>&amp;Returns</source>
        <translation>&amp;Devoluções</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="585"/>
        <location filename="m_main/desktop.ui" line="615"/>
        <source>Returns &amp;Movement</source>
        <translation>&amp;Movimento de Devoluções</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="590"/>
        <source>&amp;Requests</source>
        <translation>&amp;Pedidos</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="595"/>
        <source>Requests &amp;Control</source>
        <translation>&amp;Controle de Pedidos</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="605"/>
        <source>Budgets C&amp;ontrol</source>
        <translation>Controle de O&amp;rçamentos</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="620"/>
        <source>&amp;Stock Movement</source>
        <translation>&amp;Movimento de Estoque</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="788"/>
        <source>&amp;Stock Transfer</source>
        <translation>Tra&amp;nsferência de Estoque</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="793"/>
        <source>R&amp;ecalculate Stock Movement</source>
        <translation>Recalcular &amp;Movimento de Estoque</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="625"/>
        <source>&amp;Inventory Valuation by Date</source>
        <translation>&amp;Valoração de Estoque Por Data</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="630"/>
        <source>&amp;Product</source>
        <translation>&amp;Produtos</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="640"/>
        <source>Product &amp;Groups</source>
        <translation>&amp;Grupos de Produtos</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="655"/>
        <source>&amp;Bills to Receive</source>
        <translation>Contas a &amp;Receber</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="670"/>
        <source>C&amp;hecks</source>
        <translation>&amp;Cheques</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="675"/>
        <source>Cash &amp;Flow</source>
        <translation>Fl&amp;uxo de Caixa</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="680"/>
        <source>Bank &amp;Reconciliation</source>
        <translation>C&amp;onciliação Bancária</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="685"/>
        <source>Costs - &amp;Movement of Items</source>
        <translation>Custos - &amp;Movimento de Itens</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="690"/>
        <source>Returns - &amp;Wholesale</source>
        <translation></translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="695"/>
        <source>Returns - &amp;Retail</source>
        <translation>&amp;Devoluções</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="698"/>
        <source>Returns</source>
        <translation>Devoluções</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="703"/>
        <source>R&amp;equests</source>
        <translation>&amp;Pedidos</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="708"/>
        <source>Products Sold - &amp;Valuation</source>
        <translation>Produtos &amp;Vendidos - Valoração</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="713"/>
        <source>Products Sold - &amp;Stock</source>
        <translation>Produtos Vendidos - &amp;Estoque</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="718"/>
        <source>Requests &amp;Movement</source>
        <translation>&amp;Movimento de Pedidos</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="723"/>
        <source>&amp;Products of Pre-Sales</source>
        <translation>Pr&amp;odutos da Pré-Venda</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="728"/>
        <source>&amp;Net Profit</source>
        <translation>&amp;Lucro Líquido</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="733"/>
        <source>R&amp;anking of Sold Products</source>
        <translation>&amp;Ranking de Produtos Vendidos</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="738"/>
        <source>&amp;Control Center</source>
        <translation>&amp;Centro de Controle</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="743"/>
        <source>&amp;Local Configuration</source>
        <translation>Configurações &amp;Locais</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="748"/>
        <source>&amp;Manage Workstations</source>
        <translation>Gerenciar &amp;Estações de Trabalho</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="753"/>
        <source>&amp;Access Definition</source>
        <translation>&amp;Definição de Acesso</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="758"/>
        <source>&amp;Backup</source>
        <translation>Cópia de Seg&amp;urança</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="763"/>
        <source>Change &amp;User</source>
        <translation>&amp;Trocar Usuário</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="768"/>
        <source>Customize &amp;Toolbar</source>
        <translation>&amp;Personalizar Barra de Ferramentas</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="773"/>
        <source>Change &amp;Password</source>
        <translation>Alterar &amp;Senha</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="778"/>
        <source>&amp;Inventory Registration</source>
        <translation>&amp;Registro de Inventário</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="783"/>
        <source>Ba&amp;rcode Printing</source>
        <translation>&amp;Impressão de Código de Barras</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="798"/>
        <source>Inventory &amp;Query</source>
        <translation>C&amp;onsulta de Inventários</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="803"/>
        <source>&amp;Data Import/Export</source>
        <translation>Importação/Exporta&amp;ção de Dados</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="808"/>
        <source>A&amp;udit Monitoring</source>
        <translation>Monitoramento de A&amp;uditoria</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="813"/>
        <source>&amp;Cascade</source>
        <translation>Em &amp;Cascata</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="818"/>
        <source>Close &amp;All</source>
        <translation>&amp;Fechar Todas</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="823"/>
        <source>&amp;Main</source>
        <translation>&amp;Principal</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="828"/>
        <source>&amp;About</source>
        <translation>&amp;Sobre</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="833"/>
        <source>&amp;Content</source>
        <translation>&amp;Conteúdo</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="843"/>
        <source>&amp;Update Report</source>
        <translation>Relatório de &amp;Atualizações</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="848"/>
        <source>&amp;Exit</source>
        <translation>&amp;Sair</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="345"/>
        <source>&amp;Products</source>
        <translation>&amp;Produtos</translation>
    </message>
    <message>
        <location filename="m_main/desktop.ui" line="305"/>
        <location filename="m_main/desktop.ui" line="635"/>
        <source>&amp;Clients</source>
        <translation>&amp;Clientes</translation>
    </message>
</context>
<context>
    <name>PosClosing</name>
    <message>
        <location filename="m_financial/cashflow.ui" line="14"/>
        <source>Form</source>
        <translation>Formulário</translation>
    </message>
    <message>
        <location filename="m_financial/cashflow.ui" line="20"/>
        <source>Client</source>
        <translation>Cliente</translation>
    </message>
</context>
<context>
    <name>Product</name>
    <message>
        <location filename="m_registry/product.ui" line="26"/>
        <source>Products Registry</source>
        <translation>Cadastro de Produtos</translation>
    </message>
    <message>
        <location filename="m_registry/product.ui" line="38"/>
        <source>Price</source>
        <translation>Preço</translation>
    </message>
    <message>
        <location filename="m_registry/product.ui" line="54"/>
        <source>Factory</source>
        <translation>Fabricante</translation>
    </message>
    <message>
        <location filename="m_registry/product.ui" line="116"/>
        <source>Description</source>
        <translation>Descrição</translation>
    </message>
    <message>
        <location filename="m_registry/product.ui" line="129"/>
        <source>Code</source>
        <translation>Código</translation>
    </message>
    <message>
        <location filename="m_registry/product.ui" line="148"/>
        <location filename="m_registry/product.cpp" line="23"/>
        <source>&amp;Include</source>
        <translation>&amp;Incluir</translation>
    </message>
    <message>
        <location filename="m_registry/product.ui" line="155"/>
        <location filename="m_registry/product.cpp" line="22"/>
        <source>&amp;Change</source>
        <translation>&amp;Alterar</translation>
    </message>
    <message>
        <location filename="m_registry/product.ui" line="162"/>
        <source>&amp;Find</source>
        <translation>&amp;Localizar</translation>
    </message>
    <message>
        <location filename="m_registry/product.ui" line="169"/>
        <source>&amp;Delete</source>
        <translation>&amp;Excluir</translation>
    </message>
    <message>
        <location filename="m_registry/product.ui" line="176"/>
        <source>&amp;Close</source>
        <translation>&amp;Fechar</translation>
    </message>
    <message>
        <location filename="m_registry/product.cpp" line="25"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="m_registry/product.cpp" line="26"/>
        <source>&amp;Save</source>
        <translation>&amp;Salvar</translation>
    </message>
</context>
</TS>
